package ex.web;

import ex.NameBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class Bean implements Serializable {
    @EJB
    NameBean bean;

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return bean.getName();
    }

    public void setName() {
        bean.setName(message);
    }
}
