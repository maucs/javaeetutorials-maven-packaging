package ex;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class NameBean {
    private String name = "Initial";

    @PostConstruct
    public void init() {
        System.out.println("NameBean init-ed");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
