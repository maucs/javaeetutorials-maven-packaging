# Packaging Tutorial

In this example, the EAR package holds two WAR and one EJB module

Some observations

- The WAR modules needs the EJB dependency, yet if the WAR modules holds it (rather than the EAR package providing it to all WAR modules), a deploy error appears
    - To solve it, either set all WAR module holding EJB dependencies as provided, or use skinny wars
- The skinny war EAR configuration strips all common dependencies, making it much "lighter"
    - It has the added benefit that it prevents the problem above (without modifying WAR's dependencies)
- The Maven EAR plugin generates the `application.xml` automatically, but it can be overriden
- The WAR module's context root can be deployed to `/`
- The EJB packaging is vital at EAR deployment and when just deploying the EJB module to the Java EE server, but anything other than that, is useless
    - The EJB module itself does not have to declare packaging type
    - WAR's module dependencies do not have to declare type
- If the two WAR modules are deployed separately, they will refer to different Singletons
- The `defaultLibBundleDir` config determines the location of libraries
    - WARs and EJBs are not considered libraries

## App Client

I feel that deploying app-clients are too brittle. The idea though, is good. Any application that resides with the Java EE server can communicate using its tools. However, you can do the same with JAX-WS/RS

## Observation (Take 2)

1. EJB modules are not affected by skinny WAR, thus there are problems (when compiled in Maven)
2. Intellij's artifact creation feature **do filter out the EJB modules**
3. Intellij's artifact WAR files **must be unzipped by using the CLI `unzip` utility**

## To solve the Maven Package + Skinny WAR + EJB problem

Choose either one of these solutions

1. All WAR's EJB dependencies is `provided` scoped
2. Generate EJB clients, use them for WAR's EJB dependencies
    - Must generate `@Local` or `@Remote` classes
3. Use Intellij's generate artifact tool
    - Generate using `Build -> Build Artifacts...`
    - It also generates the EJB clients automatically (if you generate its artifact)
    
Best practice is to declare EJB's package type. Maven projects needs to declare the plugin and EJB version

## External JSF resources

Referring to **p-lib** module, all files located at `resources/META-INF/resources` will be shared among all WAR modules that includes it

Also, notice the CSS file being overridden